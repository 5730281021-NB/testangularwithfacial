export const navItems = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'Main'
    }
  },
  {
    title: true,
    name: 'Pages'
  },
  {
    name:'Tested-Page',
    url: '/HW',
    icon: 'icon-drop'
  },
  {
    name:'Tested-Page2',
    url: '/HW2',
    icon: 'icon-drop'
  },
  {
    name:'Tested-Page3',
    url: '/HW3',
    icon: 'icon-drop'
  },
  {
    name:'Tested-Page4',
    url: '/HW4',
    icon: 'icon-drop'
  }
];
