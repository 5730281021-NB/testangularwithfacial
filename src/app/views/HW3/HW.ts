import { Component } from '@angular/core';
import { navItems } from './../../_nav';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule
  ]
})
@Component({
  selector: 'app-dashboard',
  templateUrl: 'Helloworld.html'
})
export class Helloworld3 {
  public navItems = navItems;
}
